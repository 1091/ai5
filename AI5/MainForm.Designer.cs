﻿namespace AI5
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listViewObjects = new System.Windows.Forms.ListView();
            this.columnObject = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnAtributes = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.buttonObjectRemove = new System.Windows.Forms.Button();
            this.textBoxObjectName = new System.Windows.Forms.TextBox();
            this.textBoxObjectAtribute = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonObjectAdd = new System.Windows.Forms.Button();
            this.listViewConflicts = new System.Windows.Forms.ListView();
            this.column1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.column2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxConflicts1 = new System.Windows.Forms.TextBox();
            this.textBoxConflicts2 = new System.Windows.Forms.TextBox();
            this.buttonConflictsAdd = new System.Windows.Forms.Button();
            this.buttonConflictsRemove = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxSearch = new System.Windows.Forms.TextBox();
            this.buttonSearch = new System.Windows.Forms.Button();
            this.listViewRules = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label5 = new System.Windows.Forms.Label();
            this.buttonRuleRemove = new System.Windows.Forms.Button();
            this.buttonRuleAdd = new System.Windows.Forms.Button();
            this.textBoxRuleTHEN = new System.Windows.Forms.TextBox();
            this.textBoxRuleIF = new System.Windows.Forms.TextBox();
            this.buttonSave = new System.Windows.Forms.Button();
            this.buttonLoad = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // listViewObjects
            // 
            this.listViewObjects.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnObject,
            this.columnAtributes});
            this.listViewObjects.Location = new System.Drawing.Point(12, 12);
            this.listViewObjects.MultiSelect = false;
            this.listViewObjects.Name = "listViewObjects";
            this.listViewObjects.Size = new System.Drawing.Size(330, 202);
            this.listViewObjects.TabIndex = 0;
            this.listViewObjects.UseCompatibleStateImageBehavior = false;
            this.listViewObjects.View = System.Windows.Forms.View.Details;
            this.listViewObjects.SelectedIndexChanged += new System.EventHandler(this.listViewObjects_SelectedIndexChanged);
            // 
            // columnObject
            // 
            this.columnObject.Text = "Объект";
            // 
            // columnAtributes
            // 
            this.columnAtributes.Text = "Свойства";
            this.columnAtributes.Width = 266;
            // 
            // buttonObjectRemove
            // 
            this.buttonObjectRemove.Location = new System.Drawing.Point(267, 257);
            this.buttonObjectRemove.Name = "buttonObjectRemove";
            this.buttonObjectRemove.Size = new System.Drawing.Size(75, 23);
            this.buttonObjectRemove.TabIndex = 1;
            this.buttonObjectRemove.Text = "Удалить";
            this.buttonObjectRemove.UseVisualStyleBackColor = true;
            this.buttonObjectRemove.Click += new System.EventHandler(this.buttonObjectRemove_Click);
            // 
            // textBoxObjectName
            // 
            this.textBoxObjectName.Location = new System.Drawing.Point(12, 232);
            this.textBoxObjectName.Name = "textBoxObjectName";
            this.textBoxObjectName.Size = new System.Drawing.Size(103, 20);
            this.textBoxObjectName.TabIndex = 2;
            // 
            // textBoxObjectAtribute
            // 
            this.textBoxObjectAtribute.Location = new System.Drawing.Point(121, 232);
            this.textBoxObjectAtribute.Name = "textBoxObjectAtribute";
            this.textBoxObjectAtribute.Size = new System.Drawing.Size(141, 20);
            this.textBoxObjectAtribute.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 218);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Имя обекта";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(119, 217);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(126, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Свойство обекта (одно)";
            // 
            // buttonObjectAdd
            // 
            this.buttonObjectAdd.Location = new System.Drawing.Point(268, 229);
            this.buttonObjectAdd.Name = "buttonObjectAdd";
            this.buttonObjectAdd.Size = new System.Drawing.Size(74, 23);
            this.buttonObjectAdd.TabIndex = 6;
            this.buttonObjectAdd.Text = "Добавить";
            this.buttonObjectAdd.UseVisualStyleBackColor = true;
            this.buttonObjectAdd.Click += new System.EventHandler(this.buttonObjectAdd_Click);
            // 
            // listViewConflicts
            // 
            this.listViewConflicts.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.column1,
            this.column2});
            this.listViewConflicts.Location = new System.Drawing.Point(348, 13);
            this.listViewConflicts.Name = "listViewConflicts";
            this.listViewConflicts.Size = new System.Drawing.Size(347, 201);
            this.listViewConflicts.TabIndex = 7;
            this.listViewConflicts.UseCompatibleStateImageBehavior = false;
            this.listViewConflicts.View = System.Windows.Forms.View.Details;
            // 
            // column1
            // 
            this.column1.Text = "Значение";
            this.column1.Width = 176;
            // 
            // column2
            // 
            this.column2.Text = "Противоположность";
            this.column2.Width = 166;
            // 
            // label3
            // 
            this.label3.ForeColor = System.Drawing.Color.SteelBlue;
            this.label3.Location = new System.Drawing.Point(12, 258);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(233, 33);
            this.label3.TabIndex = 8;
            this.label3.Text = "Используйте регулярные выражения Пример: Больш[\\s\\S]* \\bзарплат";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // textBoxConflicts1
            // 
            this.textBoxConflicts1.Location = new System.Drawing.Point(348, 229);
            this.textBoxConflicts1.Name = "textBoxConflicts1";
            this.textBoxConflicts1.Size = new System.Drawing.Size(140, 20);
            this.textBoxConflicts1.TabIndex = 11;
            // 
            // textBoxConflicts2
            // 
            this.textBoxConflicts2.Location = new System.Drawing.Point(495, 229);
            this.textBoxConflicts2.Name = "textBoxConflicts2";
            this.textBoxConflicts2.Size = new System.Drawing.Size(125, 20);
            this.textBoxConflicts2.TabIndex = 12;
            // 
            // buttonConflictsAdd
            // 
            this.buttonConflictsAdd.Location = new System.Drawing.Point(626, 226);
            this.buttonConflictsAdd.Name = "buttonConflictsAdd";
            this.buttonConflictsAdd.Size = new System.Drawing.Size(69, 23);
            this.buttonConflictsAdd.TabIndex = 13;
            this.buttonConflictsAdd.Text = "Добавить";
            this.buttonConflictsAdd.UseVisualStyleBackColor = true;
            this.buttonConflictsAdd.Click += new System.EventHandler(this.buttonConflictsAdd_Click);
            // 
            // buttonConflictsRemove
            // 
            this.buttonConflictsRemove.Location = new System.Drawing.Point(626, 252);
            this.buttonConflictsRemove.Name = "buttonConflictsRemove";
            this.buttonConflictsRemove.Size = new System.Drawing.Size(69, 23);
            this.buttonConflictsRemove.TabIndex = 14;
            this.buttonConflictsRemove.Text = "Удалить";
            this.buttonConflictsRemove.UseVisualStyleBackColor = true;
            this.buttonConflictsRemove.Click += new System.EventHandler(this.buttonConflictsRemove_Click);
            // 
            // label4
            // 
            this.label4.ForeColor = System.Drawing.Color.SteelBlue;
            this.label4.Location = new System.Drawing.Point(360, 257);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(233, 18);
            this.label4.TabIndex = 15;
            this.label4.Text = "Используйте регулярные выражения ";
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // textBoxSearch
            // 
            this.textBoxSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxSearch.Location = new System.Drawing.Point(18, 318);
            this.textBoxSearch.Name = "textBoxSearch";
            this.textBoxSearch.Size = new System.Drawing.Size(986, 26);
            this.textBoxSearch.TabIndex = 16;
            this.textBoxSearch.Text = "Хочу престижную работу ИЛИ работу с низкой квалификацией";
            // 
            // buttonSearch
            // 
            this.buttonSearch.Location = new System.Drawing.Point(418, 350);
            this.buttonSearch.Name = "buttonSearch";
            this.buttonSearch.Size = new System.Drawing.Size(124, 41);
            this.buttonSearch.TabIndex = 17;
            this.buttonSearch.Text = "ИСКАТЬ!";
            this.buttonSearch.UseVisualStyleBackColor = true;
            this.buttonSearch.Click += new System.EventHandler(this.buttonSearch_Click);
            // 
            // listViewRules
            // 
            this.listViewRules.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this.listViewRules.Location = new System.Drawing.Point(701, 13);
            this.listViewRules.Name = "listViewRules";
            this.listViewRules.Size = new System.Drawing.Size(303, 201);
            this.listViewRules.TabIndex = 18;
            this.listViewRules.UseCompatibleStateImageBehavior = false;
            this.listViewRules.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "ЕСЛИ";
            this.columnHeader1.Width = 145;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "ТО";
            this.columnHeader2.Width = 148;
            // 
            // label5
            // 
            this.label5.ForeColor = System.Drawing.Color.Firebrick;
            this.label5.Location = new System.Drawing.Point(701, 249);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(153, 31);
            this.label5.TabIndex = 23;
            this.label5.Text = "НЕ Используйте регулярные выражения ";
            this.label5.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // buttonRuleRemove
            // 
            this.buttonRuleRemove.Location = new System.Drawing.Point(860, 252);
            this.buttonRuleRemove.Name = "buttonRuleRemove";
            this.buttonRuleRemove.Size = new System.Drawing.Size(69, 23);
            this.buttonRuleRemove.TabIndex = 22;
            this.buttonRuleRemove.Text = "Удалить";
            this.buttonRuleRemove.UseVisualStyleBackColor = true;
            this.buttonRuleRemove.Click += new System.EventHandler(this.buttonRuleRemove_Click);
            // 
            // buttonRuleAdd
            // 
            this.buttonRuleAdd.Location = new System.Drawing.Point(935, 252);
            this.buttonRuleAdd.Name = "buttonRuleAdd";
            this.buttonRuleAdd.Size = new System.Drawing.Size(69, 23);
            this.buttonRuleAdd.TabIndex = 21;
            this.buttonRuleAdd.Text = "Добавить";
            this.buttonRuleAdd.UseVisualStyleBackColor = true;
            this.buttonRuleAdd.Click += new System.EventHandler(this.buttonRuleAdd_Click);
            // 
            // textBoxRuleTHEN
            // 
            this.textBoxRuleTHEN.Location = new System.Drawing.Point(859, 226);
            this.textBoxRuleTHEN.Name = "textBoxRuleTHEN";
            this.textBoxRuleTHEN.Size = new System.Drawing.Size(145, 20);
            this.textBoxRuleTHEN.TabIndex = 20;
            // 
            // textBoxRuleIF
            // 
            this.textBoxRuleIF.Location = new System.Drawing.Point(701, 226);
            this.textBoxRuleIF.Name = "textBoxRuleIF";
            this.textBoxRuleIF.Size = new System.Drawing.Size(152, 20);
            this.textBoxRuleIF.TabIndex = 19;
            // 
            // buttonSave
            // 
            this.buttonSave.Location = new System.Drawing.Point(18, 368);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(75, 23);
            this.buttonSave.TabIndex = 24;
            this.buttonSave.Text = "Сохранить";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // buttonLoad
            // 
            this.buttonLoad.Location = new System.Drawing.Point(99, 368);
            this.buttonLoad.Name = "buttonLoad";
            this.buttonLoad.Size = new System.Drawing.Size(75, 23);
            this.buttonLoad.TabIndex = 25;
            this.buttonLoad.Text = "Загрузить";
            this.buttonLoad.UseVisualStyleBackColor = true;
            this.buttonLoad.Click += new System.EventHandler(this.buttonLoad_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1016, 406);
            this.Controls.Add(this.buttonLoad);
            this.Controls.Add(this.buttonSave);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.buttonRuleRemove);
            this.Controls.Add(this.buttonRuleAdd);
            this.Controls.Add(this.textBoxRuleTHEN);
            this.Controls.Add(this.textBoxRuleIF);
            this.Controls.Add(this.listViewRules);
            this.Controls.Add(this.buttonSearch);
            this.Controls.Add(this.textBoxSearch);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.buttonConflictsRemove);
            this.Controls.Add(this.buttonConflictsAdd);
            this.Controls.Add(this.textBoxConflicts2);
            this.Controls.Add(this.textBoxConflicts1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.listViewConflicts);
            this.Controls.Add(this.buttonObjectAdd);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxObjectAtribute);
            this.Controls.Add(this.textBoxObjectName);
            this.Controls.Add(this.buttonObjectRemove);
            this.Controls.Add(this.listViewObjects);
            this.Name = "MainForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView listViewObjects;
        private System.Windows.Forms.ColumnHeader columnObject;
        private System.Windows.Forms.ColumnHeader columnAtributes;
        private System.Windows.Forms.Button buttonObjectRemove;
        private System.Windows.Forms.TextBox textBoxObjectName;
        private System.Windows.Forms.TextBox textBoxObjectAtribute;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonObjectAdd;
        private System.Windows.Forms.ListView listViewConflicts;
        private System.Windows.Forms.ColumnHeader column1;
        private System.Windows.Forms.ColumnHeader column2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxConflicts1;
        private System.Windows.Forms.TextBox textBoxConflicts2;
        private System.Windows.Forms.Button buttonConflictsAdd;
        private System.Windows.Forms.Button buttonConflictsRemove;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxSearch;
        private System.Windows.Forms.Button buttonSearch;
        private System.Windows.Forms.ListView listViewRules;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button buttonRuleRemove;
        private System.Windows.Forms.Button buttonRuleAdd;
        private System.Windows.Forms.TextBox textBoxRuleTHEN;
        private System.Windows.Forms.TextBox textBoxRuleIF;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.Button buttonLoad;
    }
}

