﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Threading;

namespace AI5
{
    public class Interpritator
    {
        //Dictionary<string, List<string>> objects = new Dictionary<string, List<string>>();

        public List<string> getResults(Dictionary<string, List<string>> objects, 
                                        Dictionary<string, string> conflicts,
                                        Dictionary<string, string> alias,
                                            string text)
        {
           
            //Добавляем ЕСЛИ-ТО в текст
             foreach (var ali in alias.Select((Entry, Index) => new { Entry, Index }))
             {
                 //ищем ЕСЛИ добавляем ТО
                 text = text.Replace(ali.Entry.Key, (string)(" " + ali.Entry.Key +" "+ ali.Entry.Value + " "));
             }

            List<string> finalKeys = new List<string>();

            //ИЛИ
            string[] or = Regex.Split(text, "ИЛИ");
            foreach (string element in or)
            {
                List<string> possibleKeys = new List<string>();
                foreach (var x in objects.Select((Entry, Index) => new { Entry, Index }))
                {
                    foreach (string par in x.Entry.Value)
                    {
                        //Console.WriteLine("{0}: {1} = {2}", x.Index, x.Entry.Key, par);
                        //"ИЛИ текст" содержит один признаков?
                        if ((new Regex(par, RegexOptions.IgnoreCase).Matches(element).Count != 0) //содержит признак
                            || (new Regex(x.Entry.Key, RegexOptions.IgnoreCase).Matches(element).Count != 0)) // или прямое упоминание
                            //Не содержит ключ свойственный этому признаку?
                            if (!possibleKeys.Contains(x.Entry.Key))
                                possibleKeys.Add(x.Entry.Key);

                    }
                }

                //Отсеиваем противопоставления
                List<string> tempKeys = new List<string>(possibleKeys); //т.к. нельзя изменять то что в foreach
                foreach (string possibleKey in tempKeys)
                {
                    foreach (string feature in objects[possibleKey])
                    {
                        //если содержит конфликтующий признак удаляем из возможных рез-тов
                        //признак в ключе
                        if (conflicts.ContainsKey(feature)) //Такой ключ существует?
                            if (new Regex(conflicts[feature], RegexOptions.IgnoreCase).Matches(element).Count != 0)//Признак содежится?
                                possibleKeys.Remove(possibleKey);

                        //в значении ключа
                        if (conflicts.ContainsValue(feature)) //такое существует?
                            if (new Regex(conflicts.FirstOrDefault(l => l.Value == feature).Key, RegexOptions.IgnoreCase).Matches(element).Count != 0)
                            //строка содржит    ключ      найденный по значению     рвному признаку                 без учета регистра
                                possibleKeys.Remove(possibleKey);
                    }

                    //прямое упоминание другого объекта
                    foreach (var other in objects.Select((Entry, Index) => new { Entry, Index }))
                        if ((new Regex(other.Entry.Key, RegexOptions.IgnoreCase).Matches(element).Count != 0)
                            && (other.Entry.Key!=possibleKey) ) //это другой объект
                                possibleKeys.Remove(possibleKey);
                   
                       
                }
                //добавляем что осталось
                foreach (string key in possibleKeys)
                    finalKeys.Add(key);


            }

            return finalKeys;
        }


    }
}
